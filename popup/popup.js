function displayIframe(baseUrl){
    var iframe = document.createElement('iframe');
    iframe.src = baseUrl + '/ext/login';
    iframe.width = '300px';
    iframe.height = '460px';
    iframe.scrolling = 'false';
    iframe.style.border = 'none';
    var container = document.getElementById("iframe-container");
    container.appendChild(iframe);
}

main.beruby.fetchBaseUrl().then(function(baseUrl){
    displayIframe(baseUrl);
});
