var Q = require('./q');

XHR = function(method, url, options){
	if(!options) options = {async: true};
	if(typeof parseToJson == 'undefined'){ parseToJson = true; }

	return new Q.Promise(function(resolve, reject, notify){
		xhr = new XMLHttpRequest();
		xhr.open(method, url, options.async);
		xhr.onreadystatechange = function(){
			if(xhr.readyState == 4){
	            if(xhr.status == 200){
	                resolve(xhr.responseText);
	            }else{
	                reject({status: xhr.status, text: xhr.responseText});
	            }
	        }
		}

		xhr.withCredentials = true;
		xhr.setRequestHeader('x-extension-version', '1');

		xhr.send();
	});
};

module.exports = XHR;