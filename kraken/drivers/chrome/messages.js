var events = require('../../browser_events');

Messages = {
	events: events,
	send: function(action, message, cb){
		var wrapper = {action: action, message: message};
        if(!!message.tabId){
            chrome.tabs.sendMessage(message.tabId, wrapper, cb);
        }else{
            chrome.runtime.sendMessage(wrapper, cb);
        }
	},
	addListener: function(actionToListen, cb){
		chrome.runtime.onMessage.addListener(function(wrapper, sender, sendResponse){
            if(!!sender.tab){
                this.tabId = sender.tab.id;
            }
			var request = wrapper.message;
			var action = wrapper.action;
			if(actionToListen == action){
				cb.call(this, request, sender, sendResponse);
			}
			return true;
		});
		
		return true;
	},
	addBrowserListener: function(eventName, cb){
        if(eventName == events.DOCUMENT_COMPLETED){
            chrome.webNavigation.onCompleted.addListener(function(details){
                if(details.frameId === 0) {
                    cb(details);
                }
            });
        }
    }
}

module.exports = Messages;