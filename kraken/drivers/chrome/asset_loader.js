XHR = require('../../xhr');

AssetLoader = {
    loadTemplate: function(templateFile){
        return XHR('GET', templateFile);
    },
};


module.exports = AssetLoader;