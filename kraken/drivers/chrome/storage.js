var Q = require('../../q');

var Storage = {
    get: function(key){
        return Q.fcall(function(){
            return sessionStorage.getItem(key);
        });
    },
    set: function(key, value){
        return Q.fcall(function(){
            return sessionStorage.setItem(key, value);
        });
    },
    clear: function(){
        return Q.fcall(function(){
            return sessionStorage.clear();
        });
    }
};

module.exports = Storage;