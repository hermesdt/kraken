var Q = require('../../q');
if(typeof window == 'undefined')
var Request = eval('require("sdk/request")').Request;


XHR = function(method, url, options){
	if(!options) options = {async: true};
	if(typeof parseToJson == 'undefined'){ parseToJson = true; }

	return new Q.Promise(function(resolve, reject, notify){

		var request = Request({
		  url: url,
		  onComplete: function (response) {
		  	if(response.status == 200){
		  		resolve(response.text);
		  	}else{
		  		reject({status: response.status, text: response.text});
		  	}
		  }
		});
		if(method == 'GET'){
			request.get();
		}

	});
};

module.exports = XHR;