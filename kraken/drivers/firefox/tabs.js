Tabs = {
    sendMessage: function(tabId, action, data, cb){
        var wrapper = {action: action, message: data};
        chrome.tabs.sendMessage(tabId, wrapper, cb);
    }
};

module.exports = Tabs;