var Q = require("../../q");
if(typeof window == 'undefined')
var self = eval("require('sdk/self')");

AssetLoader = {
    loadTemplate: function(templateFile){
    	return new Q.Promise(function(resolve, reject, notify){
    		return resolve(self.data.load(templateFile));
    	});
    },
};


module.exports = AssetLoader;