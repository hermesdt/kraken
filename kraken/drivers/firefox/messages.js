var events = require('../../browser_events');
if(typeof window == 'undefined')
	var tabs = eval('require("sdk/tabs")');

var listeners = [];
var workers = {};

Messages = {
	events: events,
	send: function(action, message, cb){
		if(typeof self != 'undefined'){
			self.port.emit(action, message);
		}else{
			if(message.tabId){
				workers[message.tabId].port.emit(action, message);
			}
		}
	},
	addListener: function(actionToListen, cb){
		if(typeof self != 'undefined'){
			self.port.on(actionToListen, cb);
		}else{
			listeners.push({action: actionToListen, callback: cb});
		}

		// self.port.on(actionToListen, function(payload){
		// 	cb(payload, null, function(data){
		// 		Messages.send(actionToListen, data);
		// 	});
		// });
	},
    addBrowserListener: function(eventName, cb){
        if(eventName == events.DOCUMENT_COMPLETED){
            tabs.on('ready', function(tab){

                var worker = tab.attach({
                    contentScriptFile: ['./ui.js'],
                });

                var Style = eval("require('sdk/stylesheet/style').Style");
                var attach = eval("require('sdk/content/mod').attach");
                var style = Style({
				  uri: './res/css/messages.css'
				});

                attach(style, tabs.activeTab);

                attachEvents(worker, tab.id);
                workers[tab.id] = worker;
                cb({tabId: tab.id, url: tab.url});
            });

            tabs.on('close', function(tab){
            	delete workers[tab.id];
            });
        }
    }
}

function attachEvents(worker, tabId){
	for(var i = 0;i<listeners.length;i++){
		var listener = listeners[i];
		worker.port.on(listener.action, callbackBuilder(tabId, listener.callback));
	}
}

function callbackBuilder(tabId, cb){
	return function(){
		this.tabId = tabId;

		cb.apply(this, arguments);
	}
}

module.exports = Messages;