if(typeof window == 'undefined')
var ss = eval('require("sdk/simple-storage")');
var Q = require('../../q');

var Storage = {
    get: function(key){
        return new Q.Promise(function(resolve, reject, notify){
            return resolve(ss.storage[key]);
        });
    },
    set: function(key, value){
        return new Q.Promise(function(resolve, reject, notify){
            ss.storage[key] = value;
            resolve();
        });
    },
    clear: function(){
        return new Q.Promise(function(resolve, reject, notify){
            for(var property in ss.storage){
                delete ss.storage[property];
            }
            resolve();
        });
    }
};

module.exports = Storage;