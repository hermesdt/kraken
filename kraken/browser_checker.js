BrowserChecker = {
    isChrome: function(){
        return (typeof window != 'undefined') && (typeof window.chrome != 'undefined');
    }
    , isFirefox: function(){
        return (typeof window == 'undefined') || (typeof window.chrome == 'undefined');
    }
};

module.exports = BrowserChecker;