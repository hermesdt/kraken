var kraken = {};
kraken.util = {};
kraken.util.mustache = require('./mustache');
kraken.util.q = require('./q');
kraken.util.browserChecker = require('./browser_checker');
kraken.util.parseUrl = require('./url_parser');


if(kraken.util.browserChecker.isChrome()){
	kraken.xhr = require('./drivers/chrome/xhr');
	kraken.util.storage = require('./drivers/chrome/storage');;
	kraken.messages = require('./drivers/chrome/messages');
	kraken.util.assetLoader = require('./drivers/chrome/asset_loader');
    kraken.tabs = require('./drivers/chrome/tabs');
}else{
	kraken.xhr = require('./drivers/firefox/xhr');
	kraken.util.storage = require('./drivers/firefox/storage');;
    kraken.messages = require('./drivers/firefox/messages');
	kraken.util.assetLoader = require('./drivers/firefox/asset_loader');
    kraken.tabs = require('./drivers/firefox/tabs');
}

module.exports = kraken;