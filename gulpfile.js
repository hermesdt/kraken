var gulp = require('gulp');
var gulpsync = require('gulp-sync')(gulp);
var clean = require('gulp-clean');
var webpack = require('webpack-stream');
var argv = require("yargs").argv;
var rename = require("gulp-rename");

gulp.task('copy_configuration', function(){
    var config = argv.config || 'beruby';
    gulp.src('configurations/' + config + '.js')
        .pipe(rename("configuration.js"))
        .pipe(gulp.dest("."));
});

gulp.task('copy_resources', function(){
    var config = argv.config || 'beruby';
    gulp.src('configurations/' + config + '/res/**/*')
        .pipe(gulp.dest("res"));
})

gulp.task('build', gulpsync.sync(['build:kraken', 'build:main', 'build:ui']));

gulp.task('clean', function(){
    return gulp.src(['configuration.js', 'res', 'build', '../output/chrome', '../output/firefox'], {read: false})
    .pipe(clean({force: true}));
});

gulp.task('build:kraken', function() {
  return gulp.src('kraken/kraken.js')
    .pipe(webpack({
        output: {
            library: 'kraken',
            libraryType: 'var',
            filename: 'kraken.js'
        }
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('build:main', function() {
  return gulp.src('common/main.js')
    .pipe(webpack({
        output: {
            library: 'main',
            libraryType: 'var',
            filename: 'main.js'
        }
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('build:ui', function() {
  return gulp.src('common/ui.js')
    .pipe(webpack({
        output: {
            library: 'ui',
            libraryType: 'var',
            filename: 'ui.js'
        }
    }))
    .pipe(gulp.dest('build/'));
});

gulp.task('dist', gulpsync.sync(['clean', 'copy_configuration', 'copy_resources', 'build', 'dist:chrome', 'dist:firefox']));

gulp.task('dist:firefox:data', function(){
    return gulp.src(['build/ui.js'])
    .pipe(gulp.dest('../output/firefox/data'));
});

gulp.task('dist:firefox', gulpsync.sync(['dist:firefox:data', 'dist:copy:res']), function(){
    return gulp.src(['package.json', 'build/**'])
    .pipe(gulp.dest('../output/firefox/'));
});

gulp.task('dist:chrome', gulpsync.sync(['dist:copy:res', 'dist:chrome:copy:locales']), function(){
	return gulp.src(['manifest.json', 'popup/**', 'build/**'])
	.pipe(gulp.dest('../output/chrome/'));
});

gulp.task('dist:chrome:copy:locales', function(){
    return gulp.src('_locales/**', {base: '.'})
    .pipe(gulp.dest('../output/chrome/'));
});

gulp.task('dist:copy:res', function(){
	return gulp.src(['res/**'], {base: '.'})
	.pipe(gulp.dest('../output/chrome/'))
    .pipe(gulp.dest('../output/firefox/data'));
});