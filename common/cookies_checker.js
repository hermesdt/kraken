if (!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
  };
}

CookiesChecker = function(Q){
    this.Q = Q;
    this.manageCookies = function(name, value, options) {
        if (typeof value != 'undefined') { // name and value given, set cookie
            options = options || {};
            if (value === null) {
                value = '';
                options.expires = -1;
            }
            var expires = '';
            if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
                var date;
                if (typeof options.expires == 'number') {
                    date = new Date();
                    date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
                } else {
                    date = options.expires;
                }
                expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
            }
            // CAUTION: Needed to parenthesize options.path and options.domain
            // in the following expressions, otherwise they evaluate to undefined
            // in the packed version for some reason...
            var path = options.path ? '; path=' + (options.path) : '';
            var domain = options.domain ? '; domain=' + (options.domain) : '';
            var secure = options.secure ? '; secure' : '';
            document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
        } else { // only name given, get cookie
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = cookies[i].trim();
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
    };
};

CookiesChecker.prototype.checkCookies = function(){
    var deferred = this.Q.defer();

    var self = this;
    self.checkLocalCookies(deferred)
    self.checkThirdPartyCookies(deferred);

    return deferred.promise;
};

CookiesChecker.prototype.checkLocalCookies = function(deferred){
    var date = new Date();
    this.manageCookies('somecookie', date.getTime());
    if(this.manageCookies('somecookie') != date.getTime()){
        deferred.reject('localCookies');
    }

    this.manageCookies('somecookie', null);

    return deferred;
};

CookiesChecker.prototype.checkThirdPartyCookies = function(deferred){
    var image = new Image();
    image.onerror = function(){
        deferred.reject('connectionError')
    };

    image.onload = function(){
        var image2 = new Image();
        image2.onerror = function(){
            deferred.reject('thirdCookies');
        };

        image2.onload = function(){
            deferred.resolve(true);
        };

        image2.src = 'http://es.mirubi.com/check_cookie';
    };
    image.src = 'http://es.mirubi.com/set_cookie';
};

module.exports = CookiesChecker;