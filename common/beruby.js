var config = require("../configuration.js");

var Beruby = function(kraken){
    // this.BASE_URL = 'http://es.beruby.com';
    this.BASE_URL = null;
    var self = this;
    this.ignoredDomains = {};
    this.ignoredDomainsTTL = 60 * 60 * 2;
    this.lastFetchDBErrorTime = 0;
    this.lastFetchTime = 0;
    this.lastFetchTTL = 60 * 60 * 2;
    var messages = {};

    this.urlHaveCashback = function(url){
        var domain = kraken.util.parseUrl(url);

        return self.getDomainsFromDb().then(function(storedDomains){
            if(domain == null){
                return false;
            }
        
            for(var i = 0; i < storedDomains.length; i++){
                var storedDomain = storedDomains[i];
                var match = domain.search(storedDomain.domain.replace(/\./g, "\\.").replace("*", ".*"));
                if(match != -1){
                    if(!self.isIgnored(storedDomain.domain)){
                        self.sendPixel('display');
                        return storedDomain;
                    }
                }
            }

            return false;
        });
    };

    this.getNow = function(){
        return Math.round(new Date().getTime() / 1000);
    };

    this.getElapsedTime = function(time){
        return this.getNow() - time;
    };

    this.getDomainsFromDb = function(){
        return kraken.util.storage.get('domain_ids').then(function(value){
            if(value == 'error'){
                if(self.getElapsedTime(self.lastFetchDBErrorTime) < 60 * 10){
                    return [];
                }else{
                    value = null;
                }
            }
            if(self.getElapsedTime(self.lastFetchTime) >= self.lastFetchTTL){
                value = null;
            }

            if(value == null){
                console.log('[!!] fetching domains from server');
                return self.fetchData().catch(function(e){
                    kraken.util.storage.clear().then(function(){
                        kraken.util.storage.set('domain_ids', 'error');
                    });

                    self.lastFetchDBErrorTime = self.getNow();
                    console.error(e);
                    return [];
                });
            }else{
                var domainIds = JSON.parse(value);
                return kraken.util.q.all(domainIds.map(function(id){
                    return kraken.util.storage.get('domain_' + id).then(function(value){
                        return JSON.parse(value);
                    });
                }));
            }
        });
    };

    this.isIgnored = function(domain){
        var ignoredAt = Number(self.ignoredDomains[domain]) || 0;

        if(self.getElapsedTime(ignoredAt) < self.ignoredDomainsTTL){
            return true;
        }else{
            delete self.getElapsedTime[domain];
            return false;
        }
    };

    this.ignoreDomain = function(domain){
        self.ignoredDomains[domain] = self.getNow();
    };

    this.fetchData = function(){
        return self.fetchBaseUrl().then(function(baseUrl){
            return kraken.xhr("GET", baseUrl + '/ext/urls').then(function(data){
                self.lastFetchTime = self.getNow();
                return self.storeData(JSON.parse(data));
            });
        });
    };

    this.storeData = function(data){
        self.messages = {};
        data["messages"].forEach(function(row){
            self.messages[row.key] = row.message;
        });

        var ids = data["advertisers"].map(function(row){
            return row.id;
        });
        kraken.util.storage.set('domain_ids', JSON.stringify(ids));
        data["advertisers"].forEach(function(row){
            kraken.util.storage.set('domain_' + row.id, JSON.stringify(row));
        });

        return data["advertisers"].map(function(row){
            return row;
        });
    }

    this.fetchBaseUrl = function(){
        return config.getBaseUrl.call(self, kraken);
    }

    this.renderTemplate = function(templateFile, data) {
        for (var property in self.messages) {
            if (self.messages.hasOwnProperty(property)) {
                data[property] = self.messages[property];
            }
        }
        return self.fetchBaseUrl().then(function(baseUrl){
            data.baseUrl = baseUrl;
            return kraken.util.assetLoader.loadTemplate(templateFile).then(function(template){
                return kraken.util.mustache.render(template, data);
            });
        });
    };

    this.getCashback = function(advertiser){
        return self.checkLoggedIn().then(function(){
            self.sendPixel('click');

            return self.sendClickAccount(advertiser)
            .then(function(){
                return self.fetchBaseUrl();
            });
        });
    };

    this.checkLoggedIn = function(){
        return self.fetchBaseUrl().then(function(baseUrl){
            return kraken.xhr('GET', config.getIsLoggedInUrl(baseUrl));
        });
    };

    this.sendClickAccount = function(advertiser){
        return self.fetchBaseUrl().then(function(baseUrl){
            return kraken.xhr('GET', config.getClickAccountUrl(baseUrl, advertiser.id));
        });
    };

    this.sendPixel = function(type){
        return self.fetchBaseUrl().then(function(baseUrl){
            return kraken.xhr('GET', config.getPixelUrl(baseUrl, type));
        });
    };
};

if(typeof module !== 'undefined'){
    module = module || {};
    module.exports = Beruby;
}
