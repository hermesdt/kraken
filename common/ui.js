var UIController = function(options){
    var self = this;

    this.onCloseClick = function(){};
    this.onCashbackClick = function(){};
    this.onSiginClick = function(){};
    this.onSignupClick = function(){};

    if(options.onCloseClick) this.onCloseClick = options.onCloseClick;
    if(options.onCashbackClick) this.onCashbackClick = options.onCashbackClick;
    if(options.onSiginClick) this.onSiginClick = options.onSiginClick;
    if(options.onSignupClick) this.onSignupClick = options.onSignupClick;

    this.attachEvents = function(advertiser, div){
        var closeButton = div.getElementsByClassName('beruby-close')[0];
        var cashbackButton = div.getElementsByClassName('btn-cashback')[0];
        var signinButton = div.getElementsByClassName('signin')[0];
        var signupButton = div.getElementsByClassName('signup')[0];

        if(closeButton) closeButton.onclick = function(e){ self.onCloseClick(advertiser, div) };
        if(cashbackButton) cashbackButton.onclick = function(e){ self.onCashbackClick(advertiser, div) };
        if(signinButton) signinButton.onclick = function(e){ self.onSiginClick(advertiser, div) };
        if(signupButton) signupButton.onclick = function(e){ self.onSignupClick(advertiser, div) };
    };
};

var kraken = require('../kraken/kraken');
var CookiesChecker = require('./cookies_checker');

var uiController = new UIController({
    onCloseClick: function(advertiser, div){
        ignoreAdvertiser(advertiser);
        div.remove();
    },
    onCashbackClick: function(advertiser, div){
        var cookiesChecker = new CookiesChecker(kraken.util.q);
        cookiesChecker.checkCookies().then(function(){
            kraken.messages.send('get_cashback', {advertiser: advertiser});
        }).catch(function(reason){
            renderTemplate(advertiser, 'res/html/no_cookies.html');
        });
    },
    onSiginClick: function(advertiser){
        renderTemplate(advertiser, 'res/html/popup.html');
    }
});

var berubyDivId = 'beruby_popup';

kraken.messages.addListener("show_cashback", function(request, sender, sendResponse){
    displayPopup(request.advertiser, request.html);
});

kraken.messages.addListener("get_cashback_response", function(request, sender, sendResponse){
    var advertiser = request.advertiser;
    if(request.status == 'ok'){
        ignoreAdvertiser(advertiser);
        window.location.href = request.baseUrl +"/"+ advertiser.id +"/redirection?notimeout=yes";
    }else if(request.status == 'error'){
        renderTemplate(advertiser, 'res/html/not_logged.html');
    }
});

function ignoreAdvertiser(advertiser, callback){
    kraken.messages.send('ignore_advertiser', {advertiser: advertiser}, callback);
}

kraken.messages.addListener('get_template_response', function(request, sender, sendResponse){
    displayPopup(request.advertiser, request.html);
});

function renderTemplate(advertiser, template){
    kraken.messages.send("get_template", {template: template, data: advertiser});
}

function displayPopup(advertiser, html){
    if(document.getElementById(berubyDivId)){
        document.getElementById(berubyDivId).remove();
    }

    var div = document.createElement('div');
    div.id = berubyDivId;
    div.innerHTML = html;
    uiController.attachEvents(advertiser, div);
    
    document.body.appendChild(div);
};

document.addEventListener('visitedEvent', function(event){
    console.log(event);

    kraken.messages.send('visited_from_beruby', {domain: event.detail.domain}, function(){
    });
}, false);