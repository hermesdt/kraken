var kraken = require('../kraken/kraken');
var Beruby = require('./beruby');
var beruby = new Beruby(kraken);

kraken.messages.addBrowserListener(kraken.messages.events.DOCUMENT_COMPLETED, function(event){
    beruby
        .urlHaveCashback(event.url)
        .then(function(advertiser){
            if(advertiser){
                showCashback(event, advertiser);
            } 
        }).done();
});

kraken.messages.addListener('visited_from_beruby', function(request, sender, sendResponse){
    beruby.ignoreDomain(request.domain);
});

kraken.messages.addListener('ignore_advertiser', function(request, sender, sendResponse){
    beruby.ignoreDomain(request.advertiser.domain);
});

kraken.messages.addListener('get_cashback', function(request, sender, sendResponse){
    var tabId = this.tabId;
    beruby.getCashback(request.advertiser)
        .then(function(baseUrl){
            kraken.messages.send('get_cashback_response', {tabId: tabId, advertiser: request.advertiser, baseUrl: baseUrl, status: 'ok'});
        }).catch(function(e){
            kraken.messages.send('get_cashback_response', {tabId: tabId, advertiser: request.advertiser, status: 'error'});
        });
});

kraken.messages.addListener('get_template', function(request, sender, sendResponse){
    var tabId = this.tabId;
    beruby.renderTemplate(request.template, request.data || {}).then(function(html){
        kraken.messages.send('get_template_response', {tabId: tabId, advertiser: request.data, html: html});
    });
});

function showCashback(event, advertiser){
    beruby.renderTemplate('res/html/popup.html', advertiser).then(function(html){
        kraken.messages.send('show_cashback', {advertiser: advertiser, html: html, tabId: event.tabId});
    });
}

exports.beruby = beruby;