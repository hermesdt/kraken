module.exports = {
	getBaseUrl: function(kraken){
        return kraken.util.q.fcall(function(){ return 'https://ocu.beruby.com' });
    },
    getClickAccountUrl: function(baseUrl, advertiserId){
    	return baseUrl + "/click_accounts/create?widget_id=" + advertiserId;
    },
    getPixelUrl: function(baseUrl, type){
    	var country = baseUrl.match(/(..)\.beruby\.com/)[1];

    	return baseUrl + "/track/"+ 461505 +"/" + type + "/_/pixel";
    },
    getIsLoggedInUrl: function(baseUrl){
    	return baseUrl + '/account/is_logged_in';
    }
};