var PIXELS = {
    es: 481117,
    it: 93585,
    br: 96237,
    pt: 79861,
    mx: 31209,
    ar: 22269,
    co: 10441,
    cl: 4957,
    us: 40393,
    tr: 373205,
    ocu: 461505
};

module.exports = {
	getBaseUrl: function(kraken){
		var self = this;

        if(self.BASE_URL != null){
            return kraken.util.q.fcall(function(){ return self.BASE_URL });
        }

        return kraken.xhr("GET", 'http://www.beruby.com/utils/country').then(function(data){
            self.BASE_URL = JSON.parse(data)['country'];
            return self.BASE_URL;
        });
    },
    getClickAccountUrl: function(baseUrl, advertiserId){
    	return baseUrl + "/portal/mobile_click_account?widget_id=" + advertiserId;
    },
    getPixelUrl: function(baseUrl, type){
    	var country = baseUrl.match(/(..)\.beruby\.com/)[1];
        var widget_id = PIXELS[country];

    	return baseUrl + "/track/"+ widget_id +"/" + type + "/_/pixel";
    },
    getIsLoggedInUrl: function(baseUrl){
    	return baseUrl + '/account/is_logged_in';
    }
};